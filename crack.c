#include "md5.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *list2;
    // Loop through the dictionary file, one line
    // at a time.
    char *str;
    str = malloc(1000 * sizeof(char));
    // Hash each password. Compare to the target hash.
    // If they match, return the corresponding password.
    list2 = fopen(dictionary, "r+");
    while (fgets(str, 1000, list2)){
        char *nl = strchr(str, '\n');
        if (nl) * nl = '\0';

        char *hash = md5(str, strlen(str));
        if(!strcmp(hash, target)) {
            free(hash);
            fclose(list2);
            return str;
            
        }
        free(hash);

    // Free up memory?
    }
    fclose(list2);
    return NULL;
}


int main(int argc, char *argv[])
{   
    FILE *list;
    
    char str[1000];
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    list = fopen(argv[1], "r");
    while (fgets(str,1000,list)){
        char *nl = strchr(str, '\n');
        if (nl) * nl = '\0';

        char *password = crackHash(str, argv[2]);

        printf("%s %s\n", str, password);
        free(password);
    
    }
    fclose(list);
    // For each hash, crack it by passing it to crackHash
    
    // Display the hash along with the cracked password:
    //   5d41402abc4b2a76b9719d911017c592 hello
    
    // Close the hash file
    
    // Free up any malloc'd memory?
}
